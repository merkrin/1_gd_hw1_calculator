import java.math.BigInteger;

class Calculator {
    static double divide(double a, double b) {
        if (b == 0) {
            throw new IllegalArgumentException("Вы не можете делить на ноль.");
        }

        return a / b;
    }

    static double sumUp(double a, double b) {
        return a + b;
    }

    static double substract(double a, double b) {
        return a - b;
    }

    static double multiply(double a, double b) {
        return a * b;
    }

    static double mod(double a, double b) {
        if (b == 0) {
            throw new IllegalArgumentException("Вы не можете делить на ноль.");
        }

        return a % b;
    }

    static double logarithm(double number, double basis) {
        if ((basis == 1) & (number != 1)) {
            throw new IllegalArgumentException("Основание не может быть равным 1, если значение не 1. ");
        }
        if ((basis == 1) & (number == 1)) {
            throw new IllegalArgumentException("Значение Log_1(1) бесконечно велико. ");
        }
        if ((basis == 0) || (number == 0)) {
            throw new IllegalArgumentException("Значения не могут быть равны нулю. ");
        }
        if ((basis > 0) & (number < 0)) {
            throw new IllegalArgumentException("Невозможно получить отрицательное число из положительного. ");
        }

        return Math.log(number) / Math.log(basis);
    }

    static double sqrt(double number, double n) {
        if (n == 0)
            throw new IllegalArgumentException("Порядок корня не может быть 0!");
        if (n % 2 == 0 && number < 0)
            throw new IllegalArgumentException("Число под корнем не может быть отрицательным, если порядок корня четный!");
        if (number == 0)
            return 0;
        return Math.pow(number, 1.0 / n);
    }

    static double cos(double angInDeg) {
        if (angInDeg % 90 == 0)
            return 0;
        return Math.cos(Math.toRadians(angInDeg));
    }

    static double tan(double angle) {
        if (angle % 180 == 90) {
            throw new IllegalArgumentException("Тангенс не существует на этом значении");
        }
        return Math.tan(Math.toRadians(angle));
    }

    static double sin(double angle) {
        return Math.sin(Math.toRadians(angle));
    }

    static double pow(double number, double power) {
        return Math.pow(number, power);
    }

    static BigInteger factorial(int number) {
        if (number < 0)
            throw new IllegalArgumentException("Значение факториала не может быть отрицательным. ");
        BigInteger result = BigInteger.ONE;
        for (int i = 2; i <= number; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }
}
import java.util.Scanner;

public class Main {
    private static Scanner in = new Scanner(System.in);
    private static String[] operationsList = new String[]{"+", "-", "*", "/", "mod", "log", "pow", "sqrt"};
    private static String[] operationsOneArgList = new String[]{"sin", "cos", "tan", "!"};

    private static boolean doubleTryParse(String number) {
        try {
            Double.parseDouble(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static double getNumber(String name) {
        String number;
        do {
            System.out.println("Введите " + name + ":");

            number = in.nextLine();
        } while (!doubleTryParse(number));

        return Double.parseDouble(number);
    }

    private static void startCalculating(String operation, boolean hasOneArg) {
        double x, y = 0;
        if (hasOneArg) {
            if (operation.equals("sin") || operation.equals("cos") || operation.equals("tan"))
                x = getNumber("угол в градусах");
            else
                x = getNumber("число");
        }
        else {
            String[] msgs = getMsg(operation).split(":");
            x = getNumber(msgs[0]);
            y = getNumber(msgs[1]);
        }

        try {
            switch (operation) {
                case "+":
                    showAnswer(Calculator.sumUp(x, y));
                    break;
                case "-":
                    showAnswer(Calculator.substract(x, y));
                    break;
                case "*":
                    showAnswer((Calculator.multiply(x, y)));
                    break;
                case "/":
                    showAnswer(Calculator.divide(x, y));
                    break;
                case "mod":
                    showAnswer(Calculator.mod(x, y));
                    break;
                case "pow":
                    showAnswer(Calculator.pow(x, y));
                    break;
                case "sqrt":
                    showAnswer(Calculator.sqrt(x, y));
                    break;
                case "log":
                    showAnswer(Calculator.logarithm(x, y));
                    break;
                case "!":
                    System.out.println("Ответ:" + Calculator.factorial((int)x));
                    break;
                case "sin":
                    showAnswer(Calculator.sin(x));
                    break;
                case "cos":
                    showAnswer(Calculator.cos(x));
                    break;
                case "tan":
                    showAnswer(Calculator.tan(x));
                default:
                    break;
            }
        } catch (Exception e) {
            System.out.println("Ошибка выполнения. " + e.getMessage() +
                    "Проверьте правильность введённых данных.");
        }
    }

    public static void main(String[] args) {
        String operation;

        System.out.println("Добро пожаловать в \"GD-Калькулятор V0.5\"\n");

        do {
            System.out.println("Введите операцию, \"Help\" для открытия " +
                    "инструкции или \"0\" для выхода из программы:");
            operation = in.nextLine().toLowerCase();

            if (operation.equals("0")) {
                continue;
            }
            getOperation(operation);

        } while (!operation.equals("0"));
    }

    private static void getOperation(String operation) {
        if (operation.equalsIgnoreCase("help")) {
            showHelp();
        } else if (isInOperationsList(operation))
            startCalculating(operation, false);
        else if (isInOneArgOperationsList(operation))
            startCalculating(operation, true);
        else
            System.out.println("Неверный ввод операции.");
    }

    private static void showHelp() {
        System.out.println("В программе представлены 4 операции:\n" +
                "-Сложение (+)\n" +
                "-Вычитание (-)\n" +
                "-Умножение (*)\n" +
                "-Деление (/)\n" +
                "-Взятие остатка (mod)\n" +
                "-Возведение в степень (pow)\n" +
                "-Взятие корня (sqrt)\n" +
                "-Нахождение логарифма (log)\n" +
                "-Нахождение факториала (!)\n" +
                "-Нахождение синуса (sin)\n" +
                "-Нахождение косинуса (cos)\n" +
                "-Нахождение тангенса (tan)\n");
    }

    private static void showAnswer(double answer) {
        System.out.println("Ответ: " + answer);
    }

    private static boolean isInOperationsList(String checkedString) {
        for (String string : operationsList) {
            if (string.equalsIgnoreCase(checkedString)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isInOneArgOperationsList(String checkedString) {
        for (String string : operationsOneArgList) {
            if (string.equalsIgnoreCase(checkedString)) {
                return true;
            }
        }
        return false;
    }

    private static String getMsg(String op) {
        switch (op) {
            case "log":
                return "число:основание";
            case "pow":
                return "число:степень";
            case "sqrt":
                return "число:степень корня";
            default:
                return "первое число:второе число";
        }
    }
}